function ThemeDefaut() {

};

//Itialise global config
ThemeDefaut._init = function(){
	this.puceRadius = 9;
	this.titleHeight = 20;
	this.previousSize = ThemeDefaut.titleHeight;
	
	// this is the paint style for the connecting lines..
	this.connectorPaintStyle = {
		lineWidth:4,
		//strokeStyle:"#62CBC0",
		strokeStyle:"#c5c5c5",
		joinstyle:"round",
		outlineColor:"white",
		outlineWidth:2
	}

	// .. and this is the hover style. 
	this.connectorHoverStyle = {
		lineWidth:4,
		strokeStyle:"#FC6A70",
		outlineWidth:2,
		outlineColor:"white"
	};

	this.endpointHoverStyle = {
		fillStyle:"#f3565d",
		strokeStyle:"#f3565d"
	};
};
ThemeDefaut._init();



// the definition of source endpoints (the small blue ones)
ThemeDefaut.sourceEndpointStyle = function(epName)
 {
 	return {
		endpoint:"Dot",
		paintStyle:{ 
			strokeStyle:"#f3565d",
			fillStyle:"transparent",
			radius:7,
			lineWidth:3 
		},				
		isSource:true,
		connector:[ "StateMachine", { stub:[10, 10], gap:0, cornerRadius:5, alwaysRespectStubs:false } ],								                
		connectorStyle:this.connectorPaintStyle,
		hoverPaintStyle:this.endpointHoverStyle,
		connectorHoverStyle:this.connectorHoverStyle,
        dragOptions:{}
    }
},	


// the definition of target endpoints (will appear when the user drags a connection) 
ThemeDefaut.targetEndpointStyle = function(epName){
	return {
	endpoint:"Dot",					
	paintStyle:{ fillStyle:"#f3565d",radius:8 },
	hoverPaintStyle:this.endpointHoverStyle,
	maxConnections:-1,
	dropOptions:{ hoverClass:"hover", activeClass:"active" },
	isTarget:true
	}
}