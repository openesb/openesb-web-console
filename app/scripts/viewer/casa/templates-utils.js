function TemplatesUtils() {};

TemplatesUtils._loadComponentsShortName =  function(){
	this.componentShortNames  = new Array();
	this.componentShortNames['sun-ftp-binding'] = "ftp";
	this.componentShortNames['sun-database-binding'] = "db";
	this.componentShortNames['sun-email-binding'] = "email";
	this.componentShortNames['sun-jms-binding'] = "jms";
	this.componentShortNames['sun-http-binding'] = "soap";
	this.componentShortNames['sun-bpel-engine'] = "bpel";
	this.componentShortNames['sun-file-binding'] = "file";
	this.componentShortNames['sun-ldap-binding'] = "ldap";
	this.componentShortNames['sun-rest-binding'] = "rest";
	this.componentShortNames['sun-scheduler-binding'] = "sched";
	this.componentShortNames['sun-iep-binding'] = "iep";
	this.componentShortNames['sun-javaee-engine'] = "jee";
	this.componentShortNames['sun-pojo-engine'] = "pojo";
	this.componentShortNames['sun-xslt-engine'] = "xslt";	
}
TemplatesUtils._loadComponentsShortName();



TemplatesUtils.getComponentShortName = function(type){
	 if(TemplatesUtils.componentShortNames[type]){
	 	return TemplatesUtils.componentShortNames[type];
	 }
	 return "??"
};


