'use strict';

var port = window.document.location.port;
var host = window.location.host;
var proto = location.protocol;

// 9000 is dev port
if (port == 9000) {
    host = 'localhost:4848'; // Default port
}

openesbApp.service('Datastore', [
    function() {
        var _default = {
            name: 'Local instance',
            url: proto + '//' + host + '/openesb/api',
            username: '',
            password: ''
        };

        // TODO: use local storage
        this.getCurrentInstance = function() {
            return _default;
        };
    }
]);
