'use strict';

openesbApp.service('ComponentService', ['$http', '$q', 'Utils',
    function($http, $q, Utils) {
        this.findAll = function() {
            return $http.get('/components');
        };

        this.findByLibrary = function(libraryName) {
            return $http.get('/components?library='+libraryName);
        };

        this.get = function(componentName) {
            return $http.get('/components/' + componentName);
        };

        this.getDescriptor = function(componentName) {
            return $http.get('/components/' + componentName + '/descriptor');
        };

        this.getDescriptorAsXml = function(componentName) {
            var result = $q.defer();

            $http.get(
                '/components/' + componentName + '/descriptor',
                {
                    headers: {'Accept': 'application/xml'}
                }
            ).success(function(response) {
                    var descriptor = {};
                    descriptor.xml = _.escape(response.toString());
                    result.resolve(descriptor);
            });

            return result.promise;
        };

        this.getComponentConfiguration = function(componentName) {
            var result = $q.defer();

            $http.get('/components/' + componentName + '/configuration')
                .success(function(configValues) {
                    var configs = configValues;

                    $http.get('/components/' + componentName + '/schema')
                        .success(function(schema) {
                            var form = Utils.convertConfigurationToForm(schema);

                            angular.forEach(form.form_groups, function(group) {
                                angular.forEach(group.form_fields, function(field) {
                                    angular.forEach(configs, function(config) {
                                        if (field.field_name === config.name) {
                                            if (field.field_type === 'radio') {
                                                field.field_value = config.value.toString();
                                            } else {
                                                field.field_value = config.value;
                                            }
                                        }
                                    });
                                });
                            });

                            result.resolve(form);
                        });
                });

            return result.promise;
        };

        this.listApplicationConfigurations = function(componentName) {
            return $http.get('/components/' + componentName + '/application/configurations');
        };

        this.deleteApplicationConfiguration = function(componentName, applicationConfigurationName) {
            return $http.delete('/components/' + componentName + '/application/configurations/' + applicationConfigurationName);
        };

        this.getApplicationConfiguration = function(componentName, applicationConfigurationName) {
            var result = $q.defer();

            $http.get('/components/' + componentName + '/application/configurations/' + applicationConfigurationName)
                .success(function(configValues) {
                    var configs = configValues.configurations;

                    $http.get('/components/' + componentName + '/schema')
                        .success(function(schema) {
                            var form = Utils.convertApplicationConfigurationToForm(schema);

                            angular.forEach(form.form_groups, function(group) {
                                angular.forEach(group.form_fields, function(field) {
                                    angular.forEach(configs, function(config) {
                                        if (field.field_name === config.name) {
                                            if (field.field_type === 'radio') {
                                                field.field_value = config.value.toString();
                                            } else {
                                                field.field_value = config.value;
                                            }

                                            if (field.field_name === 'configurationName') {
                                                field.field_disabled = true;
                                            }
                                        }
                                    });
                                });
                            });

                            result.resolve(form);
                        });
                });

            return result.promise;
        };

        this.addComponentConfiguration = function(componentName, configurations) {
            var applicationConfiguration = {
                'name': configurations[0].value,
                'configurations': configurations
            };

            return $http.put('/components/' + componentName + '/application/configurations/', applicationConfiguration);
        };

        this.updateComponentConfiguration = function(componentName, configurations) {
            var applicationConfiguration = {
                'name': configurations[0].value,
                'configurations': configurations
            };

            return $http.post('/components/' + componentName + '/application/configurations/', applicationConfiguration);
        };

        this.saveComponentConfigurations = function(componentName, configurations) {
            return $http.put('/components/' + componentName + '/configuration', configurations);
        };

        this.getApplicationVariables = function(componentName) {
            return $http.get('/components/' + componentName + '/application/variables');
        };

        this.updateApplicationVariables = function(componentName, variables) {
            return $http.post('/components/' + componentName + '/application/variables', variables);
        };

        this.addApplicationVariables = function(componentName, variables) {
            return $http.put('/components/' + componentName + '/application/variables', variables);
        };

        this.deleteApplicationVariable = function(componentName, variableName) {
            return $http.delete('/components/' + componentName + '/application/variables?name=' + variableName);
        };

        this.getLoggers = function(componentName) {
            return $http.get('/components/' + componentName + '/loggers');
        };

        this.setLoggerLevel = function(componentName, loggerName, loggerLevel) {
            return $http.put('/components/' + componentName + '/loggers?logger='+loggerName+'&level='+loggerLevel);
        };

        this.start = function(componentName) {
            return $http.post('/components/' + componentName + '?action=start');
        };

        this.stop = function(componentName) {
            return $http.post('/components/' + componentName + '?action=stop');
        };

        this.shutdown = function(componentName) {
            return $http.post('/components/' + componentName + '?action=shutdown');
        };

        this.delete = function(componentName) {
            return $http.delete('/components/' + componentName + '?force=true');
        };

        this.getStatistics = function(componentName) {
            return $http.get('/components/' + componentName + '/stats');
        };

        this.startComponents = function(components) {
          return this.doGlobalAction('start', components);
        };

        this.stopComponents = function(components) {
          return this.doGlobalAction('stop', components);
        };

        this.shutdownComponents = function(components) {
          return this.doGlobalAction('shutdown', components);
        };

        this.doGlobalAction = function(action, components) {
          var result = $q.defer();
          var promises = [];

          for(var i = 0 ; i < components.length ; i++) {
            promises.push($http.post('/components/' + components[i] + '?action=' + action));
          }

          $q.all(promises).then(function(data){
            result.resolve(data);
          }, function(error) {
            result.reject(error);
          });

          return result.promise;
        }
    }
]);
