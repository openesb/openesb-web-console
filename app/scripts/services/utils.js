'use strict';


openesbApp.factory('Utils',
    function () {

        function formatProperty(property) {
            var newField = {
                "field_name": property.name,
                "field_title": property.displayName,
                "field_description": property.displayDescription,
                "field_type": "textfield",
                "field_value": property.defaultValue,
                "field_required": property.required
            };

            if (property.type.localPart.indexOf("int") !== -1) {
                newField.field_type = "number";
                newField.field_value = parseInt(newField.field_value);

                // Applying constraint
                if (property.Constraint !== undefined && property.Constraint != null) {
                    newField.field_constraints = {
                        "min": -2147483648,
                        "max": 2147483647
                    };

                    if (property.Constraint[0] !== undefined && property.Constraint[0] != null) {
                        newField.field_constraints.min = property.Constraint[0].value;
                    }
                    if (property.Constraint[1] !== undefined && property.Constraint[1] != null) {
                        newField.field_constraints.max = property.Constraint[1].value;
                    }
                }

            } else if (property.type.localPart.indexOf("boolean") !== -1) {
                newField.field_type = "radio";
                newField.field_options = [];

                if (newField.field_value != null) {
                  newField.field_value = newField.field_value.toString();
                }

                newField.field_options.push({
                    "option_id": "true",
                    "option_title": "True",
                    "option_value": "true"
                });

                newField.field_options.push({
                    "option_id": "false",
                    "option_title": "False",
                    "option_value": "false"
                });
            } else {
                // Common case : <input type=text>
                console.log('property (', property.name, '): ', property.type);
                if (property.encrypted === true) {
                  console.log('property.encrypted: ', property.encrypted);
                    newField.field_type = "password";
                } else if (property.Constraint !== undefined && property.Constraint != null) {
                  console.log('property.Constraint: ', property.Constraint);
                    newField.field_type = "dropdown";
                    newField.field_options = [];

                    angular.forEach(property.Constraint, function(constraint) {
                        newField.field_options.push({
                            "option_id": constraint.value,
                            "option_title": constraint.value,
                            "option_value": constraint.value
                        });
                    });
                } else if (property.type.localPart.indexOf('string') !== -1) {
                    console.log('property.type: ', property.type);
                    console.log('property.type.localPart: ', property.type.localPart);
                    newField.field_type = 'textfield';
                } else {
                  console.log("PROPERTY NOT HANDLED");
                }
              console.log("PROPERTY (",  property.name, ') FINISHED');
            }

            return newField;
        };

        return {
            convertConfigurationToForm: function(schema) {
                var form = {};
                form.form_groups = [];

                // First, create a default group for non-affected fields
                var defaultGroup = {
                    "group_name": "default",
                    "group_title": "General",
                    "group_description": "Manage component"
                };

                defaultGroup.form_fields = [];

                form.form_groups.push(defaultGroup);

                angular.forEach(schema.propertyOrPropertyGroup, function(property) {
                    if (property.PropertyGroup === undefined) {
                        var newField = formatProperty(property.Property);

                        defaultGroup.form_fields.push(newField);
                    } else {
                        var newGroup = {
                            "group_name": property.PropertyGroup.name,
                            "group_title": property.PropertyGroup.displayName,
                            "group_description": property.PropertyGroup.displayDescription
                        };
                        newGroup.form_fields = [];

                        angular.forEach(property.PropertyGroup.Property, function(grpProperty) {
                            var newField = formatProperty(grpProperty);
                            // put newField into fields array
                            newGroup.form_fields.push(newField);
                        });

                        form.form_groups.push(newGroup);
                    }
                });

                // If default group contains no field, then remove it
                if (defaultGroup.form_fields.length === 0) {
                    form.form_groups.splice(0, 1);
                }

                return form;
            },

            convertApplicationConfigurationToForm: function(schema) {
                var form = {};
                form.form_groups = [];

                // First, create a general group for all fields
                var generalGroup = {
                    "group_name": "general",
                    "group_title": "General",
                    "group_description": "Properties",
                    "form_fields": []
                };

                // Then, create an identification group for field name
                var identificationGroup = {
                    "group_name": "identification",
                    "group_title": "Identification",
                    "group_description": "",
                    "form_fields": []
                };

                form.form_groups.push(identificationGroup);
                form.form_groups.push(generalGroup);

                angular.forEach(schema.ApplicationConfiguration.Property, function(property) {
                    var newField = formatProperty(property);

                    if (newField.field_name === 'configurationName') {
                        identificationGroup.form_fields.push(newField);
                    } else {
                        generalGroup.form_fields.push(newField);
                    }
                });

                return form;
            }
        };
    }
);
