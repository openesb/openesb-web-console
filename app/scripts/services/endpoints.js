'use strict';

openesbApp.service('EndpointService', ['$http',
    function($http) {
        this.findAll = function() {
            return $http.get('/nmr/endpoints');
        };
    }
]);