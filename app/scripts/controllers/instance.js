'use strict';

var openesbInstance = angular.module('openesb.instance', [
        'ui.router'
    ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {
                $urlRouterProvider
                    .when('/instance', '/instance/general')

                $stateProvider
                    .state('instance', {
                        url: '/instance',
                        templateUrl: 'views/instance/instance.html',
                        controller: 'InstanceDetailCtrl'
                    })
                    .state('instance.general', {
                        url: '/general',
                        templateUrl: 'views/instance/instance-general.html'
                    })
                    .state('instance.system', {
                        url: '/system',
                        templateUrl: 'views/instance/instance-system-monitoring.html',
                        controller: 'InstanceSystemMonitoringCtrl'
                    })
                    .state('instance.loggers', {
                        url: '/loggers',
                        templateUrl: 'views/instance/instance-loggers.html',
                        controller: 'InstanceLoggersCtrl',
                        resolve: {
                            resolvedLoggers:['$stateParams', 'InstanceService', function ($stateParams, InstanceService) {
                                return InstanceService.getLoggers();
                            }]
                        }
                    })
                    .state('instance.monitoring', {
                        url: '/monitoring',
                        templateUrl: 'views/instance/instance-monitoring.html',
                        controller:['$scope', 'resolvedStatistics', function($scope, resolvedStatistics) {
                            $scope.statistics = resolvedStatistics.data;
                        }],
                        resolve: {
                            resolvedStatistics:['InstanceService', function (InstanceService) {
                                return InstanceService.getStatistics();
                            }]
                        }
                    });
            }
        ]
    );

openesbInstance.controller('InstanceDetailCtrl', ['$scope', '$state', '$stateParams', 'InstanceService',
    function ($scope, $state, $stateParams, InstanceService) {

        InstanceService.getInformations()
            .success(function(data) {
                $scope.instance = data;
            })
            .error(function(data) {
                $scope.error = data;
            });

        $scope.getTabClass = function(tabname) {
            return {
                'active': ($state.current.name === 'instance.'+tabname)
            }
        };
    }
]);

openesbInstance.controller('InstanceLoggersCtrl', ['$scope', 'resolvedLoggers', 'InstanceService',
    function($scope, resolvedLoggers, InstanceService) {
        $scope.loggers = resolvedLoggers.data; // DBY: why do we need .data here ?

        $scope.changeLevel = function(loggerName, newLevel) {
            InstanceService.setLoggerLevel(loggerName, newLevel)
                .success(function(data) {
                    $scope.loggers = data;
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };
    }
]);

openesbInstance.controller('InstanceSystemMonitoringCtrl', ['$scope', '$timeout', 'config', 'InstanceService',
    function ($scope, $timeout, config, InstanceService) {
        var timer;

        var refresh = function() {
            InstanceService.getJVMMetrics().then(function(response) {
                $scope.gc = response[0].data;
                $scope.memory = response[1].data;
                $scope.thread = response[2].data;
            });

            timer = $timeout(refresh, config.refresh_interval);
        }

        $scope.$on("$destroy", function() {
            if (timer) {
                $timeout.cancel(timer);
            }
        });

        refresh();
    }
]);