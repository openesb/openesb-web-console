'use strict';

var openesbLibraries = angular.module('openesb.libraries', [
        'ui.router'
    ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider
                    .when('/libraries/:libraryName', '/libraries/:libraryName/general');

                $stateProvider
                    .state('libraries', {
                        url: '/libraries',
                        templateUrl: 'views/libraries/libraries.html',
                        controller: 'LibrariesListCtrl'
                    })
                    .state('library-install', {
                        url: '/library/install',
                        templateUrl: 'views/libraries/library-install.html'
                    })
                    .state('library', {
                        url: '/libraries/:libraryName',
                        abstract: true, // Is it really needed ?
                        templateUrl: 'views/libraries/library.html',
                        controller: 'LibraryDetailCtrl'
                    })
                    .state('library.general', {
                        url: '/general',
                        templateUrl: 'views/libraries/library-general.html'
                    })
                    .state('library.descriptor', {
                        url: '/descriptor',
                        templateUrl: 'views/libraries/library-descriptor.html',
                        controller:['$scope', 'resolvedDescriptor', function($scope, resolvedDescriptor){
                            $scope.descriptor = resolvedDescriptor.xml;
                        }],
                        resolve: {
                            resolvedDescriptor:['$stateParams', 'LibraryService', function ($stateParams, LibraryService) {
                                return LibraryService.getDescriptorAsXml($stateParams.libraryName);
                            }]
                        }
                    })
                    .state('library.components', {
                        url: '/components',
                        templateUrl: 'views/libraries/library-components.html',
                        controller:['$scope', 'resolvedComponents', function($scope, resolvedComponents){
                            $scope.components = resolvedComponents.data; // DBY: why do we need .data here ?
                        }],
                        resolve: {
                            resolvedComponents:['$stateParams', 'ComponentService', function ($stateParams, ComponentService) {
                                return ComponentService.findByLibrary($stateParams.libraryName);
                            }]
                        }
                    });
            }
        ]
    );

openesbLibraries.controller('LibraryUploadController', ['$rootScope', '$scope', 'Datastore', '$cookieStore',
    function ($rootScope, $scope, Datastore, $cookieStore) {
        $scope.options = {
            url: Datastore.getCurrentInstance().url + '/libraries/',
            dataType: 'text'
        };

        $scope.$on(['fileuploadsend'], function (e, data) {
          data.headers['Authorization'] = 'Basic ' + $cookieStore.get('basicCredentials');
        });

        $('#fileupload').bind('fileuploaddone', function (e, data) {
            $rootScope.$broadcast('event:library-installed', data.result);
        }).bind('fileuploadfail', function (e, data) {
                if (data._response.jqXHR) {
                    $scope.error = data._response.jqXHR.responseText;
                }
            });
    }
]);

openesbLibraries.controller('LibrariesListCtrl', ['$scope', 'LibraryService', '$modal',
    function ($scope, LibraryService, $modal) {
        $scope.delete = function(library) {

            var modalInstance = $modal.open({
                templateUrl: 'views/libraries/library-delete.html',
                controller: 'DeleteLibraryInstanceCtrl',
                resolve: {
                    libraryName: function() {
                        return library.name;
                    }
                }
            });

            modalInstance.result.then(function() {
                LibraryService.delete(library.name)
                    .success(function(data) {
                        reload();
                    }).error(function(data) {
                        // TODO: Do something here...
                    });
                }, function() {
                    $log.info('Modal dismissed at: ' + new Date());
            });
        };

        function reload() {
            LibraryService.findAll().success(function(data) {
                $scope.libraries = data;
            });
        };

        reload();
        $scope.predicate = 'name';
    }
]);

openesbLibraries.controller('LibraryDetailCtrl', ['$scope', '$state', '$stateParams', 'LibraryService',
    function ($scope, $state, $stateParams, LibraryService) {

        $scope.library = {
            name: $stateParams.libraryName
        };

        LibraryService.get($stateParams.libraryName)
            .success(function(data) {
                $scope.library = data;
            }).error(function(data) {
                $scope.error = data;
            });

        $scope.getTabClass = function(tabname) {
            return {
                'active': ($state.current.name === 'library.'+tabname)
            }
        };
    }
]);

openesbLibraries.controller('DeleteLibraryInstanceCtrl', ['$scope', '$modalInstance', 'libraryName',
    function ($scope, $modalInstance, libraryName) {
        $scope.library = libraryName;

        $scope.ok = function() {
            $modalInstance.close($scope.library);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);
