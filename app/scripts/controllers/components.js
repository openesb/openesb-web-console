'use strict';

var openesbComponents = angular.module('openesb.components', [
        'ui.router'
    ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider
                    .when('/components/:componentName', '/components/:componentName/general');

                $stateProvider
                    .state('components', {
                        url: '/components',
                        templateUrl: 'views/components/components.html',
                        controller: 'ComponentsListCtrl'
                    })
                    .state('component-install', {
                        url: '/component/install',
                        templateUrl: 'views/components/component-install.html'
                    })
                    .state('component', {
                        url: '/components/:componentName',
                        abstract: true, // Is it really needed ?
                        templateUrl: 'views/components/component.html',
                        controller: 'ComponentDetailCtrl',
                        resolve: {
                          resolvedComponent:['$stateParams', 'ComponentService', function ($stateParams, ComponentService) {
                            return ComponentService.get($stateParams.componentName);
                          }]
                        }
                    })
                    .state('component.general', {
                        url: '/general',
                        templateUrl: 'views/components/component-general.html',
                        controller: 'ComponentGeneralCtrl'
                    })
                    .state('component.descriptor', {
                        url: '/descriptor',
                        templateUrl: 'views/components/component-descriptor.html',
                        controller:['$scope', 'resolvedDescriptor', function($scope, resolvedDescriptor) {
                            $scope.descriptor = resolvedDescriptor.xml;
                        }],
                        resolve: {
                            resolvedDescriptor:['$stateParams', 'ComponentService', function ($stateParams, ComponentService) {
                                return ComponentService.getDescriptorAsXml($stateParams.componentName);
                            }]
                        }
                    })
                    .state('component.libraries', {
                        url: '/libraries',
                        templateUrl: 'views/components/component-libraries.html',
                        controller: ['$scope', '$stateParams', 'ComponentService', function($scope, $stateParams, ComponentService) {
                            ComponentService.getDescriptor($stateParams.componentName).success(function(data) {
                                $scope.component = data;
                            });
                        }]
                    })
                    .state('component.loggers', {
                        url: '/loggers',
                        templateUrl: 'views/components/component-loggers.html',
                        controller: 'ComponentLoggersCtrl',
                        resolve: {
                            resolvedLoggers:['$stateParams', 'ComponentService', function ($stateParams, ComponentService) {
                                return ComponentService.getLoggers($stateParams.componentName);
                            }]
                        }
                    })
                    .state('component.configuration', {
                        url: '/configuration',
                        templateUrl: 'views/components/component-configuration.html',
                        controller: 'ComponentConfigurationCtrl',
                        resolve: {
                            resolvedConfiguration:['$stateParams', 'ComponentService', function ($stateParams, ComponentService) {
                                return ComponentService.getComponentConfiguration($stateParams.componentName);
                            }]
                        }
                    })
                    .state('component.appconfigurations', {
                        url: '/appconfigurations',
                        templateUrl: 'views/components/component-application-configurations.html',
                        controller: 'ComponentApplicationConfigurationsCtrl'
                    })
                    .state('component.variables', {
                        url: '/variables',
                        templateUrl: 'views/components/component-application-variables.html',
                        controller: 'ComponentApplicationVariablesCtrl'
                    })
                    .state('component.monitoring', {
                        url: '/monitoring',
                        templateUrl: 'views/components/component-monitoring.html',
                        controller: 'ComponentMonitoringCtrl'
                    })
                    .state('component.upgrade', {
                      url: '/upgrade',
                      templateUrl: 'views/components/component-upgrade.html',
                      controller: 'ComponentUpgradeCtrl'
                    });
            }
        ]
    );

openesbComponents.controller('ComponentUploadController', ['$rootScope', '$scope', 'Datastore', '$cookieStore',
    function ($rootScope, $scope, Datastore, $cookieStore) {
        $scope.options = {
            url: Datastore.getCurrentInstance().url + '/components/',
            dataType: 'text'
        };

        $scope.$on(['fileuploadsend'], function (e, data) {
          data.headers['Authorization'] = 'Basic ' + $cookieStore.get('basicCredentials');
        });

        $('#fileupload').bind('fileuploaddone', function (e, data) {
            $rootScope.$broadcast('event:component-installed', data.result);
        }).bind('fileuploadfail', function (e, data) {
                if (data._response.jqXHR) {
                    $scope.error = data._response.jqXHR.responseText;
                }
            });
    }
]);

openesbComponents.controller('ComponentUpgradeCtrl', ['$rootScope', '$scope', 'Datastore', '$cookieStore',
  function ($rootScope, $scope, Datastore, $cookieStore) {
    $scope.options = {
      url: Datastore.getCurrentInstance().url + '/components/' + $scope.component.name,
      type: 'PUT',
      dataType: 'text'
    };

    $scope.$on(['fileuploadsend'], function (e, data) {
      data.headers['Authorization'] = 'Basic ' + $cookieStore.get('basicCredentials');
    });

    $('#fileupload').bind('fileuploaddone', function (e, data) {
      $rootScope.$broadcast('event:component-installed', $scope.component.name);
    }).bind('fileuploadfail', function (e, data) {
      if (data._response.jqXHR) {
        $scope.error = data._response.jqXHR.responseText;
      }
    });
  }
]);

openesbComponents.controller('ComponentsListCtrl', ['$scope', 'ComponentService', '$modal', 'toaster',
    function ($scope, ComponentService, $modal, toaster) {

        $scope.checkboxes = {'checked': false, items: {}};
        // watch for check all checkbox
        $scope.$watch('checkboxes.checked', function(value) {
          angular.forEach($scope.components, function(component) {
            if (angular.isDefined(component.name)) {
              $scope.checkboxes.items[component.name] = value;
            }
          });
        });

        $scope.delete = function(component) {
            var modalInstance = $modal.open({
                templateUrl: 'views/components/component-delete.html',
                controller: 'DeleteComponentInstanceCtrl',
                resolve: {
                    componentName: function() {
                        return component.name;
                    }
                }
            });

            modalInstance.result.then(function() {
                ComponentService.delete(component.name).success(function(data) {
                    reload();
                    toaster.pop('success', 'Component deleted', component.name + ' has been deleted successfully.', 3000);
                }).error(function(data) {
                    toaster.pop('error', 'Unable to delete ' + component.name, data);
                        // TODO: Do something here...
                    });
            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.startSelected = function() {
          doMultipleAction('start');
        };

        $scope.stopSelected = function() {
          doMultipleAction('stop');
        };

        $scope.shutdownSelected = function() {
          doMultipleAction('shutdown');
        };

        function doMultipleAction(actionName) {
          var components = [];

          switchActionButtons(false);

          angular.forEach($scope.components, function(component) {
            if ($scope.checkboxes.items[component.name] == true) {
              components.push(component.name);
            }
          });

          if (components.length > 0) {
            var promise;
            if (actionName === 'start') {
              promise = ComponentService.startComponents(components);
            } else if (actionName === 'stop') {
              promise = ComponentService.stopComponents(components);
            } else if (actionName === 'shutdown') {
              promise = ComponentService.shutdownComponents(components);
            }

            if (promise !== undefined) {
              promise.then(function(data) {
                reload();
                toaster.pop('success', 'Components ' + actionName, 'Components ' + components + ' have been ' + actionName + 'ed successfully.', 3000);
              }, function(error) {
                reload();
                toaster.pop('error', 'Unable to perform ' + actionName + ' action for ' + components);
              });
            }
          }
        }

        function switchActionButtons(enabled) {
          document.getElementById("start-selected-btn").disabled = !enabled;
          document.getElementById("stop-selected-btn").disabled = !enabled;
          document.getElementById("shutdown-selected-btn").disabled = !enabled;
        }

        function uncheckMaster() {
          $scope.checkboxes.checked = false;
          angular.forEach($scope.components, function(component) {
            if (angular.isDefined(component.name)) {
              $scope.checkboxes.items[component.name] = false;
            }
          });
        }

        function reload() {
            uncheckMaster();
            switchActionButtons(true);
            ComponentService.findAll().success(function(data) {
                $scope.components = data;
            });
        };

        reload();
        $scope.predicate = 'name';
    }
]);

openesbComponents.controller('ComponentDetailCtrl', ['$scope', '$state', '$stateParams', 'ComponentService', 'resolvedComponent',
    function ($scope, $state, $stateParams, ComponentService, resolvedComponent) {
        $scope.component = resolvedComponent.data;

        $scope.getTabClass = function(tabname) {
            return {
                'active': ($state.current.name === 'component.'+tabname)
            }
        };

        $scope.$on('event:component-lifecycle', function(event, component) {
            ComponentService.get(component)
                .success(function(data) {
                    $scope.component = data;
                })
                .error(function(data) {
                    $scope.error = data;
                });
        });
    }
]);

openesbComponents.controller('ComponentGeneralCtrl', ['$scope', '$location', '$modal', '$stateParams', 'toaster', 'ComponentService',
    function ($scope, $location, $modal, $stateParams, toaster, ComponentService) {
        $scope.start = function() {
            ComponentService.start($scope.component.name)
                .success(function(data) {
                    reload();
                    toaster.pop('success', 'Component started', $scope.component.name + ' has been started successfully.', 3000);
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };

        $scope.stop = function() {
            ComponentService.stop($scope.component.name)
                .success(function(data) {
                    reload();
                    toaster.pop('success', 'Component stopped', $scope.component.name + ' has been stopped successfully.', 3000);
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };

        $scope.shutdown = function() {
            ComponentService.shutdown($scope.component.name)
                .success(function(data) {
                    reload();
                    toaster.pop('success', 'Component shutdown', $scope.component.name + ' has been shutdown successfully.', 3000);
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };

        $scope.delete = function() {

          var modalInstance = $modal.open({
            templateUrl: 'views/components/component-delete.html',
            controller: 'DeleteComponentInstanceCtrl',
            resolve: {
              componentName: function() {
                return $scope.component.name;
              }
            }
          });

          modalInstance.result.then(function() {
            ComponentService.delete($scope.component.name).success(function(data) {
              $location.path('/components').replace();
              toaster.pop('success', 'Component deleted', $scope.component.name + ' has been deleted successfully.', 3000);
            }).error(function(data) {
              toaster.pop('error', 'Unable to delete ' + $scope.component.name, data);
            });
          }, function() {
            console.log('Modal dismissed at: ' + new Date());
          });

        };

        function reload() {
            $scope.$emit('event:component-lifecycle', $scope.component.name);
        };
    }
]);

openesbComponents.controller('ComponentLoggersCtrl', ['$scope', 'resolvedLoggers', 'ComponentService',
    function($scope, resolvedLoggers, ComponentService) {
        $scope.loggers = resolvedLoggers.data; // DBY: why do we need .data here ?

        $scope.changeLevel = function(loggerName, newLevel) {
            ComponentService.setLoggerLevel($scope.component.name, loggerName, newLevel)
                .success(function(data) {
                    $scope.loggers = data;
                })
                .error(function(data) {
                    $scope.error = data;
                });
        };
    }
]);

openesbComponents.controller('ComponentConfigurationCtrl', ['$scope', 'resolvedConfiguration', 'toaster', 'ComponentService',
    function($scope, resolvedConfiguration, toaster, ComponentService) {
        $scope.form = resolvedConfiguration;

        $scope.saveForm = function() {
            var configurations = [];
            angular.forEach($scope.form.form_groups, function(group) {
                angular.forEach(group.form_fields, function(field) {
                    if (field.field_type === 'radio') {
                        configurations.push({
                            name: field.field_name,
                            value: (field.field_value === 'true')
                        });
                    } else {
                        configurations.push({
                            name: field.field_name,
                            value: field.field_value
                        });
                    }
                });
            });

            ComponentService.saveComponentConfigurations($scope.component.name, configurations)
                .success(function(data) {
                  toaster.pop('success', 'Component configuration', 'Configuration has been successfully set. Please restart the component !', 3000);
                })
                .error(function(data) {
                  toaster.pop('error', 'Component configuration', 'An error occurs while saving configuration: '+data, 3000);
                });
        };
    }
]);

openesbComponents.controller('ComponentApplicationConfigurationsCtrl', ['$scope', '$modal', 'ComponentService',
    function($scope, $modal, ComponentService) {

        if ($scope.component.supportApplicationConfigurations === true) {
            $scope.checkboxes = {'checked': false, items: {}};

            // watch for check all checkbox
            $scope.$watch('checkboxes.checked', function(value) {
                angular.forEach($scope.configurations, function(config) {
                    if (angular.isDefined(config)) {
                        $scope.checkboxes.items[config] = value;
                    }
                });
            });

            ComponentService.listApplicationConfigurations($scope.component.name)
                .success(function(data) {
                    uncheckMaster();
                    $scope.configurations = data;
                });
        }

        $scope.deleteApplicationConfigurations = function() {
            angular.forEach($scope.configurations, function(config) {
                if ($scope.checkboxes.items[config] == true) {
                    ComponentService.deleteApplicationConfiguration($scope.component.name, config)
                        .success(function(data) {
                            uncheckMaster();
                            $scope.configurations = data;
                        });
                }
            });
        }

        $scope.getApplicationConfiguration = function(applicationConfigurationName) {
            uncheckMaster();

            ComponentService.getApplicationConfiguration($scope.component.name, applicationConfigurationName)
                .then(function(data) {
                    uncheckMaster();

                    var modalInstance = $modal.open({
                        templateUrl: 'views/components/add-application-configuration.html',
                        controller: 'ManageApplicationConfigurationCtrl',
                        resolve: {
                            component: function() {
                                return $scope.component;
                            },
                            applicationConfiguration: function() {
                                return data;
                            },
                            name: function() {
                                return applicationConfigurationName;
                            }
                        }
                    });

                    modalInstance.result.then(function(applicationConfigurationForm) {
                        var applicationConfiguration = extractApplicationConfiguration(applicationConfigurationForm);
                        ComponentService.updateComponentConfiguration($scope.component.name, applicationConfiguration)
                            .success(function(data) {
                            //    $scope.variables = data;
                            });
                    }, function() {
                        // Nothing to do here....
                    });
                }, function() {
                    console.log('error');
                });
        };

        $scope.addApplicationConfiguration = function() {
            ComponentService.getApplicationConfiguration($scope.component.name, '')
                .then(function(data) {
                    uncheckMaster();

                    var modalInstance = $modal.open({
                        templateUrl: 'views/components/add-application-configuration.html',
                        controller: 'ManageApplicationConfigurationCtrl',
                        resolve: {
                            component: function() {
                                return $scope.component;
                            },
                            applicationConfiguration: function() {
                                return data;
                            },
                            name: function() {
                                return undefined;
                            }
                        }
                    });

                    modalInstance.result.then(function(applicationConfigurationForm) {
                        var applicationConfiguration = extractApplicationConfiguration(applicationConfigurationForm);
                        ComponentService.addComponentConfiguration($scope.component.name, applicationConfiguration)
                            .success(function() {
                              ComponentService.listApplicationConfigurations($scope.component.name)
                                .success(function(data) {
                                  uncheckMaster();
                                  $scope.configurations = data;
                                });
                            });
                    }, function() {
                        // Nothing to do here....
                    });
                }, function() {
                    console.log('error');
                });
        };

        function uncheckMaster() {
            $scope.checkboxes.checked = false;
        }

        function extractApplicationConfiguration(formData) {
            var configurations = [];
            angular.forEach(formData.form_groups, function(group) {
                angular.forEach(group.form_fields, function(field) {
                    if (field.field_type === 'radio') {
                        configurations.push({
                            name: field.field_name,
                            value: (field.field_value === 'true')
                        });
                    } else {
                        configurations.push({
                            name: field.field_name,
                            value: field.field_value
                        });
                    }
                });
            });

            return configurations;
        }
    }
]);

openesbComponents.controller('ComponentApplicationVariablesCtrl', ['$scope', '$modal', 'ComponentService',
    function($scope, $modal, ComponentService) {

        if ($scope.component.supportApplicationVariables === true) {
                $scope.checkboxes = {'checked': false, items: {}};

                // watch for check all checkbox
                $scope.$watch('checkboxes.checked', function(value) {
                    angular.forEach($scope.variables, function(item) {
                        if (angular.isDefined(item.name)) {
                            $scope.checkboxes.items[item.name] = value;
                        }
                    });
                });

            ComponentService.getApplicationVariables($scope.component.name)
                .success(function(data) {
                    uncheckMaster();
                    $scope.variables = data;
                });
        }

        $scope.saveApplicationVariables = function() {
            uncheckMaster();

          var update_array = [];
          if($scope.formVariables.$pristine === false){
            // if any value from from changes then

            $scope.variables.map(function (variable) {

              // if that variable changed the value then push it into update_array
              var changed = !$scope.formVariables[variable.name].$pristine;
              if (changed) update_array.push(variable);
            });

            // send the update_array with updated values
            ComponentService.updateApplicationVariables($scope.component.name, update_array)
              .success(function (data) {
                $scope.variables = data;
              });
          }

        };

        $scope.deleteApplicationVariables = function() {
            angular.forEach($scope.variables, function(item) {
                if ($scope.checkboxes.items[item.name] == true) {
                    ComponentService.deleteApplicationVariable($scope.component.name, item.name)
                        .success(function(data) {
                            uncheckMaster();
                            $scope.variables = data;
                        });
                }
            });
        }

        $scope.addApplicationVariable = function() {
            uncheckMaster();

            var variable = {
                'name': '',
                'type': 'STRING',
                'value': ''
            };

            var modalInstance = $modal.open({
                templateUrl: 'views/components/add-application-variable.html',
                controller: 'AddApplicationVariableCtrl',
                resolve: {
                    component: function() {
                        return $scope.component;
                    },
                    applicationVariable: function() {
                        return variable;
                    }
                }
            });

            modalInstance.result.then(function(applicationVariable) {
                var variables = new Array();
                variables.push(applicationVariable);

                ComponentService.addApplicationVariables($scope.component.name, variables)
                    .success(function(data) {
                        $scope.variables = data;
                    });
            }, function() {
                // Nothing to do here....
            });
        };

        function uncheckMaster() {
            $scope.checkboxes.checked = false;
        }
    }
]);

openesbComponents.controller('AddApplicationVariableCtrl', ['$scope', '$modalInstance', 'component', 'applicationVariable',
    function ($scope, $modalInstance, component, applicationVariable) {
        $scope.component = component;
        $scope.variable = applicationVariable;

        $scope.ok = function() {
            $modalInstance.close($scope.variable);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

openesbComponents.controller('ManageApplicationConfigurationCtrl', ['$scope', '$modalInstance', 'component', 'applicationConfiguration', 'name',
    function ($scope, $modalInstance, component, applicationConfiguration, name) {
        $scope.component = component;
        $scope.applicationConfiguration = applicationConfiguration;
        $scope.name = name;

        $scope.ok = function() {
            $modalInstance.close($scope.applicationConfiguration);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

openesbComponents.controller('DeleteComponentInstanceCtrl', ['$scope', '$modalInstance', 'componentName',
    function ($scope, $modalInstance, componentName) {
        $scope.component = componentName;

        $scope.ok = function() {
            $modalInstance.close($scope.component);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

openesbComponents.controller('ComponentMonitoringCtrl', ['$scope', '$modal', 'ComponentService',
    function($scope, $modal, ComponentService) {
        ComponentService.getStatistics($scope.component.name)
            .success(function(data) {
                $scope.statistics = data;
            });
    }
]);
